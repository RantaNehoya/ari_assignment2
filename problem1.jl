### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ a4b7ab70-cb2d-11ec-37ca-ef874883575d
using Pkg, PlutoUI, DataStructures, Statistics

# ╔═╡ 33353f3f-1027-43f9-9608-03deee50c20e
md"## Floors and Offices"

# ╔═╡ bfb8adfc-1c61-4114-afeb-e85effe22fc1
begin
	floors = "2"
	offices = "3"
	intFloors = parse(Int, floors)
	intOffices = parse(Int, offices)
end;

# ╔═╡ a874308f-1c76-4c24-9dac-35fef477b1c7
md"#### Return Arrays"

# ╔═╡ cf3c4937-f59e-4dbb-b49e-777413f9d141
begin
	oCount = 1
	office = []

	while oCount <= intOffices
		push!(office, oCount)
		oCount += 1
	end

	fCount = 1
	floor = []

	while fCount <= intFloors
		push!(floor, fCount)
		fCount += 1
	end
end

# ╔═╡ 13f4871e-201c-4913-ae6e-84502617d3f1
md"#### Company Layout"

# ╔═╡ 53a12c22-1ad8-4f93-8ebd-304c5441c578
begin
	build = []
	for i in floor
		subBuild = []
		for j in office
			push!(subBuild, j)
		end
		push!(build, subBuild)
	end
	
	itNum = intOffices
	for (ind, k) in enumerate(build)
		if ind == 1
			continue
		end
	
		for (index, num) in enumerate(build[ind])
			build[ind][index] = num + itNum
		end
		
		itNum += intOffices
	end
	build
end

# ╔═╡ f3aa049f-e3a8-41ca-bced-477fff4912d3
md"## Define Action"

# ╔═╡ 5b59610b-f52e-4fc0-8c12-461fcfbdedd7
struct Action
	name::String
	cost::Int64
end

# ╔═╡ 8dbc2e59-73c0-4b50-a48c-654c566d5cc1
begin
	me = Action("move east", 2)
	mw = Action("move west", 2)
	mu = Action("move up", 1)
	md = Action("move down", 1)
	co = Action("collect", 5)
end;

# ╔═╡ 49269db9-a750-4fa3-8e55-47b04afc9282
md"## Define States"

# ╔═╡ 155d90ed-84f6-4d35-8ae4-c5a59bce9286
mutable struct Office
	roomNumber::String
	floor::String
	destination::String
	numParcel::Int
	hasAgent::Bool
end

# ╔═╡ 6695d22c-3e3b-4964-8904-7dfb7bd19f3b
struct Node
	state::Office
	parent::Union{Nothing, Node}
	action::Union{Nothing, Action}
end

# ╔═╡ 63835e6b-ae93-451e-a673-e62079ce4fd9
md"## Company Floor Layout"

# ╔═╡ 7bfd5d3e-e372-4787-a917-fc988d377838
begin
	off = OrderedDict()
	lastValue = build[intFloors][intOffices]
	
	for (i, flr) in enumerate(build)
		for o in build[i]
			
			off[o] = Office(string(o), string(i), string(rand(1:lastValue)), rand(1:5), 0)
			
		end
	end
end

# ╔═╡ bb9dec66-dab6-4565-bc30-5e24558d4e49
begin
	floorOffices = OrderedDict()
	for (i, k) in enumerate(build)
		floorArr = []
		
		for j in build[i]
			push!(floorArr, off[j])
		end
		
		floorOffices[i] = floorArr
	end
end

# ╔═╡ f7b06a4d-675a-401d-80a9-902e83aa9f8a
md"## Transition Model"

# ╔═╡ 16ff5ae8-1ddb-4379-9830-a97e617340f6
build

# ╔═╡ 03bd9ada-580f-4d40-94b3-f49e1128f972
agentStartPos = build[rand(1:intFloors)][rand(1:intOffices)]

# ╔═╡ 6fa9ecb6-86e2-4743-98d4-a88c517b1c41
begin
	action = Dict()
	function transModel(node::Node)
		agentLoc = missing
		
		for (key, value) in enumerate(floorOffices)

			for (k, v) in enumerate(floorOffices)
				agentLoc = floorOffices[key][k]
				if agentLoc.floor == floorOffices[key][k].floor

					if agentStartPos == parse(Int, off[key].roomNumber)
						
						floorOffices[key][k].hasAgent = true
		
						#collect
						action[co] = nodes(
							Office(
								floorOffices[key][k].floor,
								floorOffices[key][k].roomNumber,
								floorOffices[key][k].destination,
								floorOffices[key][k].numParcel > 0 ? floorOffices[key][k].numParcel -= 1 : 0,
								floorOffices[key][k].hasAgent
							),
							node,
							co
						)
						
					end

					#horizontal movement/transition
					#move east
					if agentLoc.roomNumber < last(floorOffices[k]).roomNumber
						if agentLoc.roomNumber == floorOffices[key][k].roomNumber

							floorOffices[key][k].hasAgent = false
							floorOffices[key][k + 1].hasAgent = true
						
							action[me] = nodes(
								Office(
									floorOffices[key][k + 1].floor,
									floorOffices[key][k + 1].roomNumber,
									floorOffices[key][k + 1].destination,
									floorOffices[key][k + 1].numParcel,
									floorOffices[key][k + 1].hasAgent
								),
								node,
								me
							)
						end
					end

					#move west
					if agentLoc.roomNumber > floorOffices[k][1].roomNumber && k > 1
						if parse(Int, agentLoc.roomNumber) == parse(Int, floorOffices[key][k].roomNumber)

								floorOffices[key][k].hasAgent = false
								floorOffices[key][k - 1].hasAgent = true
						
								action[mw] = Node(
									Office(
										floorOffices[key][k - 1].floor,
										floorOffices[key][k - 1].roomNumber,
										floorOffices[key][k - 1].destination,
										floorOffices[key][k - 1].numParcel,
										floorOffices[key][k - 1].hasAgent
									),
									Node,
									mw
								)
						end
					end

					#vertical movement/transition
					#move up
					if parse(Int, agentLoc.floor) < length(floorOffices)
						if agentLoc.roomNumber == floorOffices[key][k].roomNumber

							floorOffices[key][k].hasAgent = false
							floorOffices[key + 1][k].hasAgent = true
					
							action[mu] = Node(
								Office(
									floorOffices[key + 1][k].floor,
									floorOffices[key + 1][k].roomNumber,
									floorOffices[key + 1][k].destination,
									floorOffices[key + 1][k].numParcel,
									floorOffices[key + 1][k].hasAgent
								),
								Node,
								mu
							)
						end
					end

					#move down
					if parse(Int, agentLoc.floor) > 1 && key > 1
					
						if agentLoc.roomNumber == floorOffices[key][k].roomNumber

							floorOffices[key][k].hasAgent = false
							floorOffices[key - 1][k].hasAgent = true
						
							action[md] = Node(
								Office(
									floorOffices[key - 1][k].floor,
									floorOffices[key - 1][k].roomNumber,
									floorOffices[key - 1][k].destination,
									floorOffices[key - 1][k].numParcel,
									floorOffices[key - 1][k].hasAgent
								),
								Node,
								md
							)
						end
					end
				end
			end
		end
		return action
	end
end;

# ╔═╡ 09cbcbbd-8758-45bf-8998-233f8149ccb8
md"## Goal State"

# ╔═╡ 6a1a9d3b-0679-4d45-b476-4e7f04261556
function isGoalState()
	goalArr = []
	for (key, value) in enumerate(floorOffices)
		
		for j in floorOffices[key]
			if j.numParcel > 0
				push!(goalArr, j.numParcel)
			end
		end
	end

	return isempty(goalArr)
end;

# ╔═╡ a42e8358-64f3-4697-b25c-c22c8f0cc4da
function path_(node::Node)
	p = [node]
	while !isnothing(node.parent)
		node = node.parent
		pushfirst!(p, node)
	end
	return p
end;

# ╔═╡ caa8e952-791f-40c1-a908-9b881bb48555
function sol(node::Node, walked::Array)
	p = path_(node)
	actions = []
	
	for node in p
		if !isnothing(node.action)
			push!(actions, node.action)
		end
	end

	cost = length(actions)
	
	return cost, "Found $(length(actions)) step solution in $(length(walked)) steps: $(join(actions, " -> "))"
end;

# ╔═╡ f6df4848-9f2c-48f3-a0ab-a9f7af329b31
function failed(note::String)
	return note
end;

# ╔═╡ bc9412ec-8cbe-4460-a4ef-ae2699d4c6e9
md"## Heuristic Function"

# ╔═╡ 806ab631-cc0a-4985-974d-09912f0d6972
function retParcels()
	parcLeft = 0
	for (key, value) in enumerate(floorOffices)
		
		for j in floorOffices[key]
			parcLeft += j.numParcel
		end
	end
	return parcLeft
end;

# ╔═╡ a1e7a6ee-ca75-4d25-9952-67713a035dbb
#totCost = me.cost + mw.cost + mu.cost + md.cost + co.cost

# ╔═╡ d5721ba3-9a5c-413c-964e-e4f7fbc324f2
function heuristicFunction(parcLeft)
	G = 0
	
	h = parcLeft * 3

	f = G + h

	return f
end;

# ╔═╡ dd9a4bdb-56ad-42e2-b978-632a966d06f8
heuristicFunction(retParcels())

# ╔═╡ 4baab2b5-7dd5-4269-8556-78d99ac8e57c
md"## A* Search"

# ╔═╡ d47dbfd2-7179-4ad3-b81c-8e66408a72b1
function calcPathCost(node::Node)::Int
	return length(path_(node))
end;

# ╔═╡ c64bfee2-b767-4d3c-bcf5-41d1231d576b
function aStarCost(node::Node)
	return calcPathCost(node) + heuristicFunction(retParcels())
end;

# ╔═╡ 70c70839-a9e1-40b1-870c-8d162866c514
function aStarSearch(start::Office)
	node = Node(start, nothing, nothing)
	if isGoalState()
		return sol(node, "Goal already reached", [])
	end
	frontier = PriorityQueue{Node, Int}()
	enqueue!(frontier, node, heuristicFunction(retParcels()))
	explored = []
	count = 0
	while true
		if isempty(frontier)
			return failed("Failed to find solution")
		end
		node = dequeue!(frontier)
		push!(explored, node.state)
		#for (action, child) in transModel(node)
			#if !(child in keys(frontier)) && !(child in explored)
			#	if isGoalState()
			#		return solution(child, explored)
			#	end
			#	enqueue!(frontier, child, a_star_cost(child))
			#end
		#end
		if count > 100
			return failed("Timed out")
		end
	end
end;

# ╔═╡ 0bedea93-54a5-4216-b2c0-6ab505c6ab44
initState = floorOffices[rand(1:length(floorOffices))][rand(1:length(floorOffices))]

# ╔═╡ 3e3ed66c-b118-4dfa-9b71-b93d676e0612
aStarSearch(initState)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
Pkg = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Statistics = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═a4b7ab70-cb2d-11ec-37ca-ef874883575d
# ╟─33353f3f-1027-43f9-9608-03deee50c20e
# ╠═bfb8adfc-1c61-4114-afeb-e85effe22fc1
# ╟─a874308f-1c76-4c24-9dac-35fef477b1c7
# ╠═cf3c4937-f59e-4dbb-b49e-777413f9d141
# ╟─13f4871e-201c-4913-ae6e-84502617d3f1
# ╠═53a12c22-1ad8-4f93-8ebd-304c5441c578
# ╟─f3aa049f-e3a8-41ca-bced-477fff4912d3
# ╠═5b59610b-f52e-4fc0-8c12-461fcfbdedd7
# ╠═8dbc2e59-73c0-4b50-a48c-654c566d5cc1
# ╟─49269db9-a750-4fa3-8e55-47b04afc9282
# ╠═155d90ed-84f6-4d35-8ae4-c5a59bce9286
# ╠═6695d22c-3e3b-4964-8904-7dfb7bd19f3b
# ╟─63835e6b-ae93-451e-a673-e62079ce4fd9
# ╠═7bfd5d3e-e372-4787-a917-fc988d377838
# ╠═bb9dec66-dab6-4565-bc30-5e24558d4e49
# ╟─f7b06a4d-675a-401d-80a9-902e83aa9f8a
# ╠═16ff5ae8-1ddb-4379-9830-a97e617340f6
# ╠═03bd9ada-580f-4d40-94b3-f49e1128f972
# ╠═6fa9ecb6-86e2-4743-98d4-a88c517b1c41
# ╟─09cbcbbd-8758-45bf-8998-233f8149ccb8
# ╠═6a1a9d3b-0679-4d45-b476-4e7f04261556
# ╠═a42e8358-64f3-4697-b25c-c22c8f0cc4da
# ╠═caa8e952-791f-40c1-a908-9b881bb48555
# ╠═f6df4848-9f2c-48f3-a0ab-a9f7af329b31
# ╟─bc9412ec-8cbe-4460-a4ef-ae2699d4c6e9
# ╠═806ab631-cc0a-4985-974d-09912f0d6972
# ╠═a1e7a6ee-ca75-4d25-9952-67713a035dbb
# ╠═d5721ba3-9a5c-413c-964e-e4f7fbc324f2
# ╠═dd9a4bdb-56ad-42e2-b978-632a966d06f8
# ╟─4baab2b5-7dd5-4269-8556-78d99ac8e57c
# ╠═d47dbfd2-7179-4ad3-b81c-8e66408a72b1
# ╠═c64bfee2-b767-4d3c-bcf5-41d1231d576b
# ╠═70c70839-a9e1-40b1-870c-8d162866c514
# ╠═0bedea93-54a5-4216-b2c0-6ab505c6ab44
# ╠═3e3ed66c-b118-4dfa-9b71-b93d676e0612
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
